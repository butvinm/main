section .data
codes:
    db '0123456789ABCDEF'
newline:
    db 10


section .text

; Exit program
exit:
    mov  rax, 60
    xor  rdi, rdi
    syscall


; Print line break to stdout
print_newline:
    mov rax, 1 ; write
    mov rdi, 1 ; stdout
    mov rdx, 1 ; one char
    mov rsi, 10
    push rsi
    mov rsi, rsp
    syscall
    pop rsi
    ret


; Print hexidecimal number to stdout
; Inputs: rdi - number
print_hex:
    mov rax, rdi ; put number to rax

    mov rdi, 1 ; stdout descriptor
    mov rdx, 1
    mov rcx, 64
    ; Each 4 bits should be output as one hexadecimal digit
    ; Use shift and bitwise AND to isolate them
    ; the result is the offset in 'codes' array
    .loop:
        push rax
        sub rcx, 4
        ; cl is a register, smallest part of rcx
        ; rax -- eax -- ax -- ah + al
        ; rcx -- ecx -- cx -- ch + cl
        sar rax, cl
        and rax, 0xf

        lea rsi, [codes + rax]
        mov rax, 1

        ; syscall leaves rcx and r11 changed
        push rcx
        syscall
        pop rcx

        pop rax
        ; test can be used for the fastest 'is it a zero?' check
        ; see docs for 'test' command
        test rcx, rcx
        jnz .loop

    ret


; Get length of null-terminated string.
;
; Inputs: rdi - Address of null-terminated string.
; Outputs: rax - Length of string in bytes (without null-terminator).
string_length:
    mov rax, rdi
    .counter:
        cmp byte [rdi], 0
        je .end
        inc rdi
        jmp .counter
    .end:
        sub rdi, rax
        mov rax, rdi
        ret


; Print null-terminated string.
;
; Inputs: rdi - Address of null-terminated string.
; Outputs: rax - Number of printed bytes or -1 on error.
print_string:
    push rdi ; save string address
    call string_length ; calculate string length
    mov rdx, rax ; put string length to rdx
    pop rsi ; pop string address from stack to rsi
    mov rax, 1 ; write syscall
    mov rdi, 1 ; stdout
    syscall
    ret


; Parse unsigned integer from null-terminated string
;
; Inputs: rdi - Null-terminated string address.
; Outputs: rax - Parsed integer.
;          rdx - Count of parsed digits. 0 if error occured.
parse_uint:
    mov rax, 0
    mov rdx, 0
    mov rsi, 0 ; current char

    .digit:
        mov sil, [rdi + rdx] ; read char

        ; check non-integer
        cmp sil, '0'
        jb .end
        cmp sil, '9'
        ja .end

        ; happy path
        imul rax, rax, 10
        sub sil, '0'
        add rax, rsi
        inc rdx

        jmp .digit

    .end:
        ret
