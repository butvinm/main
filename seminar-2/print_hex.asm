; print_hex.asm
section .data
codes:
    db '0123456789ABCDEF'
newline:
    db 10

section .text
global _start

; Print hexidecimal number to stdout
; Inputs: rdi - number
print_hex:
    mov rax, rdi ; put number to rax

    mov rdi, 1 ; stdout descriptor
    mov rdx, 1
    mov rcx, 64
    ; Each 4 bits should be output as one hexadecimal digit
    ; Use shift and bitwise AND to isolate them
    ; the result is the offset in 'codes' array
    .loop:
        push rax
        sub rcx, 4
        ; cl is a register, smallest part of rcx
        ; rax -- eax -- ax -- ah + al
        ; rcx -- ecx -- cx -- ch + cl
        sar rax, cl
        and rax, 0xf

        lea rsi, [codes + rax]
        mov rax, 1

        ; syscall leaves rcx and r11 changed
        push rcx
        syscall
        pop rcx

        pop rax
        ; test can be used for the fastest 'is it a zero?' check
        ; see docs for 'test' command
        test rcx, rcx
        jnz .loop

    ret

; Print line break to stdout
print_newline:
    mov rax, 1 ; write
    mov rdi, 1 ; stdout
    mov rdx, 1 ; one char
    push 10
    mov rsi, rsp
    syscall
    pop rax
    ret


_start:
    mov rdi, 0x1122334455667788
    call print_hex
    call print_newline

    mov rdi, 0x0
    call print_hex
    call print_newline

    mov rdi, 0x0987654321
    call print_hex
    call print_newline

    mov rax, 60 ; invoke 'exit' system call
    xor rdi, rdi
    syscall
