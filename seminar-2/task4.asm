section .data
message: db '0', 0

section .text
global _start
%include "lib.inc"

_start:
    mov rdi, message
    call parse_uint
    push rdx
    push rax

    pop rdi
    call print_hex

    call print_newline

    pop rdi
    call print_hex

    call exit
