section .text

%include "lib.inc"

global _start


; Store 3 local values on stack,
; print them and restore stack.
foo:
    sub rsp, 8 * 3
    mov qword [rsp], 0xAA
    mov qword [rsp + 8], 0xBB
    mov qword [rsp + 8 * 2], 0xCC

    mov rdi, [rsp]
    call print_hex
    call print_newline

    mov rdi, [rsp + 8]
    call print_hex
    call print_newline

    mov rdi, [rsp + 8 * 2]
    call print_hex
    call print_newline

    add rsp, 8 * 3
    ret


_start:
    call foo

    call exit
